#### NOTES ON PROJ FILE STRUCTURE

- `playbooks` dir is used by Red Hat's Ansible VSCode extension to locate playbooks.

- After writing playbooks in `playbooks` dir, moved them to root proj to be able to automate provisioning AND configuration of servers using "terraform apply" from `terraform-ansible-handover-demo` proj.
  - See `terraform-ansible-handover-demo` > `main.tf` > `local-exec` provisioner > `working_dir`. This dir must have all Ans configuration files needed to run the playbook:
    - applicable Inventory files. In this case, we're passing inventory using `--inventory` flag, bypassing `hosts` file in this dir.
    - `ansible.cfg` to suppress host key checking when Ans SSH's into newly created EC2
    - any `vars_files` used in playbook
  - Keeping playbook YAMLs in `playbooks` dir w/o `ansible.cfg` prevented Ans from executing the playbook.
